/*
 * This file is auto-generated.  DO NOT MODIFY.
 * Using: out/host/linux-x86/bin/aidl --lang=java -Weverything -Wno-missing-permission-annotation -Werror -t --min_sdk_version platform_apis --ninja -d out/soong/.intermediates/system/tools/aidl/aidl_test_loggable_interface-java-source/gen/android/aidl/loggable/Enum.java.d -o out/soong/.intermediates/system/tools/aidl/aidl_test_loggable_interface-java-source/gen -Nsystem/tools/aidl/tests system/tools/aidl/tests/android/aidl/loggable/Enum.aidl
 *
 * DO NOT CHECK THIS FILE INTO A CODE TREE (e.g. git, etc..).
 * ALWAYS GENERATE THIS FILE FROM UPDATED AIDL COMPILER
 * AS A BUILD INTERMEDIATE ONLY. THIS IS NOT SOURCE CODE.
 */
package android.aidl.loggable;
public @interface Enum {
  public static final byte FOO = 42;
}
